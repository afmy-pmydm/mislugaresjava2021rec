package com.example.mislugares.modelo;

public class GeoPuntoAlt extends GeoPunto{

    private double altura;
    GeoPunto geoPunto;

    public GeoPuntoAlt(double altura, double latitud, double longitud){
        super(latitud, longitud);
        this.altura = altura;

    }

    public double distancia(GeoPuntoAlt punto){
        double distanciaOriginal = Math.pow(geoPunto.distancia(punto),2);
        double diferenciaAlturas = Math.pow(altura - punto.altura,2);
        return Math.sqrt(distanciaOriginal + diferenciaAlturas);
    }

}
