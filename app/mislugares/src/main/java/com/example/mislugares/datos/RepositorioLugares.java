package com.example.mislugares.datos;

import com.example.mislugares.modelo.Lugar;

public interface RepositorioLugares {

    void delete(int id);

    Lugar get_element(int pos);

    void update_element(int id, Lugar nuevoLugar);


    int size();
}
