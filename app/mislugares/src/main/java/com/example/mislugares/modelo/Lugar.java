package com.example.mislugares.modelo;

public class Lugar {
    private String nombre;
    private String direccion;
    private GeoPunto posicion;
    private TipoLugar tipo;
    private String foto;
    private int telefono;
    private String url;
    private String comentario;
    private long fecha;
    private float valoracion;

    public Lugar(String nombre, String direccion, double latitud, double longitud, String foto, TipoLugar tipo, int telefono, String url, String comentario,                int valoracion) {
        fecha = System.currentTimeMillis();
        posicion = new GeoPunto(latitud, longitud);
        this.foto=foto;
        this.tipo=tipo;
        this.nombre = nombre;
        this.direccion = direccion;
        this.telefono = telefono;
        this.url = url;
        this.comentario = comentario;
        this.valoracion = valoracion;
    }

    //constructor alternativo recibiendo GeoPunto en lugar de latitud y longitud
    public Lugar(String nombre, String direccion, GeoPunto p, String foto,    TipoLugar tipo, int telefono, String url, String comentario, int valoracion) {
        this(nombre,direccion, p.getLatitud(), p.getLongitud(),foto,
                tipo, telefono, url, comentario, valoracion);
    }


    public Lugar()
    {
        this("","",0,0,"",TipoLugar.OTROS,0,"","",0);
    }

    public static TipoLugar getTipo(){
        return getTipo();
    }
    public static String getNombre(){
        return getNombre();
    }
    public static String getDireccion(){
        return getDireccion();
    }
    public static int getTelefono(){
        return getTelefono();
    }
    public static String getUrl(){
        return getUrl();
    }
    public static String getComentario(){
        return getComentario();
    }

    public float getValoracion() {
        return 0;
    }

    public GeoPunto getPosicion() {
        return posicion;
    }

    public String setFoto(String uri){
        return uri;
    }

    public String getFoto(){
        return url;
    }
}

