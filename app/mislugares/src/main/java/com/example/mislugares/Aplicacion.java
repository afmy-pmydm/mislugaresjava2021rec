package com.example.mislugares;

import android.app.Application;
import android.content.res.Configuration;

import com.example.mislugares.datos.LugaresLista;
import com.example.mislugares.datos.RepositorioLugares;
import com.example.mislugares.presentacion.AdaptadorLugares;

public class Aplicacion extends Application {

    private RepositorioLugares lugares = new LugaresLista();
    @Override public void onCreate() {
        super.onCreate();
    }

    public RepositorioLugares getLugares() {
        return lugares;
    }

    public AdaptadorLugares adaptador = new AdaptadorLugares(lugares);


}
